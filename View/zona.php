<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>




<table class="table table-striped">
    <thead>
        <tr>
            <th >Numero de zona</th>
			<th >Nombre</th>
            <th>Latitud</th>
            <th>longitud</th>
            <th >Detalle</th>
             <th ><img src="Assets/img/editar.png" border="1" width="30" height="30"/></th>
            <th ><img src="Assets/img/eliminar.png" border="1" width="30" height="30"/></th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->N_Zona; ?></td>
		<td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->latitud; ?></td>
            <td><?php echo $r->longitud; ?></td>
			<td><?php echo $r->detalle; ?></td>
            <td>
                <a href="?controller=Zona&accion=Crud&N_Zona=<?php echo $r->N_Zona; ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=Zona&accion=Del&N_Zona=<?php echo $r->N_Zona; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
<div>
    <a href="?controller=Zona&accion=Crud">Nueva Zona</a>
</div>