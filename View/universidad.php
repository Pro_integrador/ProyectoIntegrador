<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>




<table class="table table-striped">
    <thead>
        <tr>
            <th >iduniversidad</th>
            <th>nombre</th>
            <th>detalle</th>
            <th ><img src="Assets/img/editar.png" border="1" width="30" height="30"/></th>
            <th ><img src="Assets/img/eliminar.png" border="1" width="30" height="30"/></th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->iduniversidad; ?></td>
            <td><?php echo $r->nombre; ?></td>
            <td><?php echo $r->detalle; ?></td>
            <td>
                <a href="?controller=universidad&accion=Crud&iduniversidad=<?php echo $r->iduniversidad; ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=universidad&accion=Del&iduniversidad=<?php echo $r->iduniversidad; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
<div>
    <a href="?controller=universidad&accion=Crud">Universidad</a>
</div>