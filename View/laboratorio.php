<?php

?>




<table class="table table-striped">
    <thead>
        <tr>
            <th >ID Laboratorio</th>
            <th>latitud</th>
            <th>longitud</th>
			 <th>detalle</th>
            <th ><img src="Assets/img/editar.png" border="1" width="30" height="30"/></th>
            <th ><img src="Assets/img/eliminar.png" border="1" width="30" height="30"/></th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->N_Laboratorio; ?></td>
            <td><?php echo $r->latitud; ?></td>
            <td><?php echo $r->longitud; ?></td>
			 <td><?php echo $r->detalle; ?></td>
            <td>
                <a href="?controller=Laboratorio&accion=Crud&N_Laboratorio=<?php echo $r->N_Laboratorio; ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=Laboratorio&accion=Del&N_Laboratorio=<?php echo $r->N_Laboratorio; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
<div>
    <a href="?controller=Laboratorio&accion=Crud">Nuevo Laboratorio</a>
</div>