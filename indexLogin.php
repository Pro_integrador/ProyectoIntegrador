<?php
    session_start();
    require_once("Login/admin.php");
    $admin = new Admin();

     if($admin->is_loggedin()!="")
    {
            $admin->redirect('indexHome.php');
    }

    if(isset($_POST['login']))
    {
            $user = $_POST['user'];
            $pass = $_POST['pass'];

            if($admin->Login($user,$pass))
            {
                    $admin->redirect('indexHome2.php');
            }
            else
            {
                $error = "Datos Erroneos!";
            }	
    }
?>


<html lang="en">
<style type="text/css"> 
body{ 
 background-image: url(Assets/img/fondo2.jpg);
	  background-position: center center;	  
	  background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
} 

</style> 
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Universidad Gerardo Barrios</title>

    <link href="Assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/small-business.css" rel="stylesheet">

  </head>

<body id=body> 

<div class="container">  
 
  <form class="form-signin" method="post" id="login-form">

               <h1 style="color:white"><center>Inicio de Sesion</center</h1><hr />

            <div id="error">
            <?php
                            if(isset($error))
                            {
                                    ?>
                    <div class="alert alert-danger">
                       <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                    </div>
                    <?php
                            }
                    ?>
            </div>

            <div class="form-group">
            <input type="text" class="form-control" name="user" placeholder="Usuario" required />            
            </div>

            <div class="form-group">
            <input type="password" class="form-control" name="pass" placeholder="Contraseña" required />
            </div>

            <hr />

            <div class="form-group">
                <button type="submit" name="login" class="btn btn-primary">
                    <i class="glyphicon glyphicon-log-in"></i> &nbsp; Iniciar Sesion
                </button>
            </div>  
          
          </form>
 
</div>
<br/><br/><br/><br/><br/>
 
<?php
    include_once 'footer.php';
?>


