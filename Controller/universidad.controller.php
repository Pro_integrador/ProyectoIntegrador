<?php



require_once 'model/universidad.php';

class universidadController{
    
    private $model;
    
    public function __construct(){
        $this->model = new universidad();
    }    
    public function Index(){
        require_once 'view/universidad.php';
       
    }    
    public function Crud(){
        $data = new universidad();
        
        if(isset($_REQUEST['iduniversidad'])){
            $data = $this->model->getByID($_REQUEST['iduniversidad']);
        }       
        require_once 'view/universidad-editar.php';          
    }  
    public function add(){
        $data = new universidad();
        
        $data->iduniversidad = $_REQUEST['iduniversidad'];
        $data->nombre = $_REQUEST['nombre'];
        $data->detalle = $_REQUEST['detalle'];

        $data->iduniversidad > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexUniversidad.php');
    }
    public function del(){
        $this->model->del($_REQUEST['iduniversidad']);
        header('Location: indexUniversidad.php');
    }

   
}