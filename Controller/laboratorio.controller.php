<?php



require_once 'model/laboratorio.php';

class LaboratorioController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Laboratorio();
    }    
    public function Index(){
        require_once 'view/laboratorio.php';
       
    }    
    public function Crud(){
        $data = new Laboratorio();
        
        if(isset($_REQUEST['N_Laboratorio'])){
            $data = $this->model->getByID($_REQUEST['N_Laboratorio']);
        }       
        require_once 'view/LaboratorioEditar.php';          
    }  
    public function add(){
        $data = new Laboratorio();
        
        $data->N_Laboratorio = $_REQUEST['N_Laboratorio'];
        $data->latitud = $_REQUEST['latitud'];
        $data->longitud = $_REQUEST['longitud'];
		$data->detalle = $_REQUEST['detalle'];

        $data->N_Laboratorio > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexLaboratorio.php');
    }
    public function del(){
        $this->model->del($_REQUEST['N_Laboratorio']);
        header('Location: indexLaboratorio.php');
    }



   
}