<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'model/edificio.php';

class EdificioController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Edificio();
    }    
    public function Index(){
        require_once 'view/edificio.php';
       
    }    
    public function Crud(){
        $data = new Edificio();
        
        if(isset($_REQUEST['N_edificio'])){
            $data = $this->model->getByID($_REQUEST['N_edificio']);
        }       
        require_once 'view/edificio-editar.php';          
    }  
    public function add(){
        $data = new Edificio();
        
        $data->N_edificio = $_REQUEST['N_edificio'];
        $data->latitud = $_REQUEST['latitud'];
        $data->longitud = $_REQUEST['longitud'];
		$data->detalle = $_REQUEST['detalle'];

        $data->N_edificio > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexEdificio.php');
    }
       public function del(){
        $this->model->del($_REQUEST['N_edificio']);
        header('Location: indexEdificio.php');
    }
    
   
}