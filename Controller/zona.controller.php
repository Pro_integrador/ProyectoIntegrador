<?php


require_once 'model/zona.php';

class ZonaController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Zona();
    }    
    public function Index(){
        require_once 'view/zona.php';
       
    }    
    public function Crud(){
        $data = new Zona();
        
        if(isset($_REQUEST['N_Zona'])){
            $data = $this->model->getByID($_REQUEST['N_Zona']);
        }       
        require_once 'view/zona-editar.php';          
    }  
    public function add(){
        $data = new Zona();
		
        
        $data->N_Zona = $_REQUEST['N_Zona'];
		   $data->nombre = $_REQUEST['nombre'];
        $data->latitud = $_REQUEST['latitud'];
        $data->longitud = $_REQUEST['longitud'];
		$data->detalle = $_REQUEST['detalle'];

        $data->N_Zona > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexZonasx.php');
    }
    
    public function del(){
        $this->model->del($_REQUEST['N_Zona']);
        header('Location: indexZonasx.php');
    }
}