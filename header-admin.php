<?php

	require_once("Login/session.php");	
	require_once("Login/Admin.php");
	$admin = new Admin();
	
	
	$id_admin = $_SESSION['sesion'];	
        $datosA = $admin->getAdmin($id_admin);
		
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Universidad Gerardo Barrios</title>

    <link href="Assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/small-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="indexHome.php">Universidad Gerardo Barrios</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="indexHome.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="administracion.php">administracion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
			 <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">User:  <?php echo $datosA['user']; ?>
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil">Perfil</a></li>  
                          <li><a href="Login/logout?logout=true">Cerrar Sesion</a></li>                         
                        </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>



     
   

   
   