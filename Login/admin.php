<?php

include_once 'database.php';

class Admin
{	

	private $pdo;
	
	public function __construct()
	{
		$db = DatabaseLogin::Conectar();
		$this->pdo = $db;
        }
	
	public function consulta($sql)
	{
		$stmt = $this->pdo->prepare($sql);
		return $stmt;
	}
	
     
	public function ListarUsuarios($user)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT user  FROM admin WHERE user=:user");
			$stmt->execute(array(':user'=>$user));

			return $datosA=$stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
        public function getAdmin($id_admin)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT id_admin, user, pass FROM admin WHERE id_admin=:id_admin");
			$stmt->execute(array(':id_admin'=>$id_admin));

			return $datosA=$stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function login($user)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT id_admin, user, pass FROM admin WHERE user=:user ");
			$stmt->execute(array(':user'=>$user));
			$datosA=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if( $datosA['pass'])
				{
					$_SESSION['sesion'] = $datosA['id_admin'];
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['sesion']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['sesion']);
		return true;
	}
}
?>