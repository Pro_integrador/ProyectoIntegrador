<?php
	require_once('session.php');
	require_once('admin.php');
	$admin = new Admin();
	
	if($admin->is_loggedin()!="")
	{
		$admin->redirect('../indexHome.php');
	}
	if(isset($_GET['logout']) && $_GET['logout']=="true")
	{
		$admin->doLogout();
		$admin->redirect('../index.php');
	}
