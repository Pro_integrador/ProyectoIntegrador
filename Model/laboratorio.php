<?php


include_once("database.php");

class Laboratorio{
    
    private $pdo;    
    public $N_Laboratorio;
    public $latitud;
    public $longitud;
	 public $detalle;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT N_Laboratorio, latitud, longitud, detalle FROM laboratorios");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($N_Laboratorio)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT N_Laboratorio, latitud, longitud, detalle FROM laboratorios WHERE N_Laboratorio = ?");
			          

			$stm->execute(array($N_Laboratorio));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
public function del($N_Laboratorio)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM laboratorios WHERE N_Laboratorio = ?");			          

		$stm->execute(array($N_Laboratorio));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE laboratorios SET 
						latitud = ?, 
						longitud = ?,
						detalle = ?
				    WHERE N_Laboratorio = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->latitud, 
                        $data->longitud,
						$data->detalle,
                        $data->N_Laboratorio
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Laboratorio $data)
	{
		try{
		$sql = "INSERT INTO laboratorios(latitud,longitud,detalle) 
		        VALUES (?, ?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->latitud, 
                    $data->longitud,
					$data->detalle
					
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}