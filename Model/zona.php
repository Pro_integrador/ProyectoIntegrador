<?php


 
include_once("database.php");

class Zona{
	
    
    private $pdo;    
    public $N_Zona;
	public $nombre;
    public $latitud;
    public $longitud;
    public $detalle;
	
	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT N_Zona,nombre, latitud, longitud, detalle FROM zonas");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($N_Zona)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT N_Zona,nombre, latitud, longitud, detalle FROM zonas WHERE N_Zona = ?");
			          

			$stm->execute(array($N_Zona));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

public function del($N_Zona)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM zonas WHERE N_Zona = ?");			          

			$stm->execute(array($N_Zona));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE zonas SET 
			            nombre = ?, 
						latitud = ?, 
						longitud = ?, 
						detalle = ?
				    WHERE N_Zona = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
					    $data->nombre, 
                        $data->latitud, 
                        $data->longitud,
						$data->detalle,
                        $data->N_Zona
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Zona $data)
	{
		try{
		$sql = "INSERT INTO zonas(nombre,latitud,longitud,detalle) 
		        VALUES (?, ?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
				    $data->nombre, 
                    $data->latitud, 
                    $data->longitud, 
                    $data->detalle
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}