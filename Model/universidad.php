<?php


 
include_once("database.php");

class universidad{
    
    private $pdo;    
    public $iduniversidad;
    public $nombre;
    public $detalle;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT iduniversidad, nombre, detalle FROM universidad");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($iduniversidad)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT iduniversidad, nombre, detalle FROM universidad WHERE iduniversidad = ?");
			          

			$stm->execute(array($iduniversidad));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

public function del($iduniversidad)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM universidad WHERE iduniversidad = ?");			          

			$stm->execute(array($iduniversidad));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE universidad SET 
						nombre = ?, 
						detalle = ?
				    WHERE iduniversidad = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->nombre, 
                        $data->detalle,
                        $data->iduniversidad
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(universidad $data)
	{
		try{
		$sql = "INSERT INTO universidad(nombre,detalle) 
		        VALUES (?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombre, 
                    $data->detalle
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}