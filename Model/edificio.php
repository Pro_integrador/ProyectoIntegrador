<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of alumno
 *
 * @author GiselaE
 */
 
include_once("database.php");

class Edificio{
    
    private $pdo;    
    public $N_edificio;
    public $latitud;
    public $longitud;
    public $detalle;
	
	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT N_edificio, latitud, longitud, detalle FROM edificio");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($N_edificio)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT N_edificio, latitud, longitud, detalle FROM edificio WHERE N_edificio = ?");
			          

			$stm->execute(array($N_edificio));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

public function del($N_edificio)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM edificio WHERE N_edificio = ?");			          

			$stm->execute(array($N_edificio));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE edificio SET 
						latitud = ?, 
						longitud = ?, 
						detalle = ?
				    WHERE N_edificio = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->latitud, 
                        $data->longitud,
						$data->detalle,
                        $data->N_edificio
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Edificio $data)
	{
		try{
		$sql = "INSERT INTO edificio(latitud,longitud,detalle) 
		        VALUES (?, ?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->latitud, 
                    $data->longitud, 
                    $data->detalle
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}