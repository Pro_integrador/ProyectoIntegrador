<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Small Business - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="Assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="Assets/css/small-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Universidad Gerardo Barrios</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="administracion.php">administracion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
  

  <div class="container">
  <br/>  <br/>  
<div class="col-lg-12">
  <h1>Menu de opciones</h1>
</div>
		
<div class="row my-4">
<div class="col-lg-6">
<div class="row my-2">
<form action="indexUniversidad.php" method="post">
<input type=image src="Assets/img/botonUniversidad.png" width="500" height="120"> 
</form>

</div>

</div>
   
<div class="col-lg-4">	
  <div class="row my-2">
   <form action="indexEdificio.php" method="post">
    <input type=image src="Assets/img/botonEdificio.png" width="500" height="120">
   </form>
  </div>		
 </div>
</div>
 
 <div class="row my-4">
<div class="col-lg-6">
<div class="row my-2">
<form action="indexAulas.php" method="post">
<input type=image src="Assets/img/botonAula.png" width="500" height="120"> 
</form>

</div>

</div>
   
<div class="col-lg-4">	
  <div class="row my-2">
   <form action="indexLaboratorio.php" method="post">
    <input type=image src="Assets/img/botonLaboratorio.png" width="500" height="120">
   </form>
  </div>		
 </div>
</div>
 
 
 <div class="row my-4">
<div class="col-lg-6">
<div class="row my-2">
<form action="indexZonasx.php" method="post">
<input type=image src="Assets/img/botonZonasX.png" width="500" height="120"> 
</form>

</div>

</div>
   

</div>
 
 
</div>
 



     
   

   
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="Assets/vendor/jquery/jquery.min.js"></script>
    <script src="Assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>